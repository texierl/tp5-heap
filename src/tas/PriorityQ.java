package tas;

public class PriorityQ<T> {

    Heap<ElemWithPriority<T>> h;

    public PriorityQ(boolean isMaxPQ) {
        h = new Heap<>(isMaxPQ);
    }

    public int size() {
        return h.size();
    }

    public void add(T e, double p) {
        ElemWithPriority<T> elemWithPriority = new ElemWithPriority<>(e, p);
        h.add(elemWithPriority);
    }

    public T getTop() {
        ElemWithPriority<T> elemWithPriority = h.getTop();
        return elemWithPriority.getElem();
    }

    public T removeTop() {
        ElemWithPriority<T> elemWithPriority = h.removeTop();
        return elemWithPriority.getElem();
    }
}
